package operations;

import logic.models.Monomial;
import logic.operations.MonomialOperations;
import org.junit.Test;

public class MonomialIntegrateTest {

    @Test
    public void integrate() {
        Monomial monomial = new Monomial(7, 2);
        MonomialOperations.integrate(monomial);
        System.out.println(monomial.toString());
    }
}