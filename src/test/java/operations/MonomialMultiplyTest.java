package operations;

import logic.models.Monomial;
import logic.operations.MonomialOperations;
import org.junit.Test;

public class MonomialMultiplyTest {

    @Test
    public void multiply() {
        Monomial firstMonomial = new Monomial(3, 1);
        Monomial secondMonomial = new Monomial(4, 5);
        Monomial result = MonomialOperations.multiply(firstMonomial, secondMonomial);
        System.out.println(result.toString());
    }
}