package operations;

import logic.models.Monomial;
import logic.operations.MonomialOperations;
import org.junit.Test;

public class MonomialDifferentiationTest {

    @Test
    public void differentiate() {
        Monomial monomial = new Monomial(7, 2);
        MonomialOperations.differentiate(monomial);
        System.out.println(monomial.toString());
    }
}