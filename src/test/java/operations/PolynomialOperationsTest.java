package operations;

import logic.models.Monomial;
import logic.models.Polynomial;
import logic.operations.PolynomialOperations;
import org.junit.Test;

public class PolynomialOperationsTest {

    @Test
    public void add() {
        Polynomial firstPolynomial = new Polynomial();
        Polynomial secondPolynomial = new Polynomial();
        Monomial monomial = new Monomial(3, 4);
        PolynomialOperations.addMonomial(firstPolynomial, monomial);
        Monomial monomial1 = new Monomial(6, 3);
        PolynomialOperations.addMonomial(firstPolynomial, monomial1);
        Monomial monomial2 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(secondPolynomial, monomial2);
        Monomial monomial3 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(secondPolynomial, monomial3);
        Monomial monomial4 = new Monomial(3, 7);
        PolynomialOperations.addMonomial(secondPolynomial, monomial4);
        Polynomial result = PolynomialOperations.add(firstPolynomial, secondPolynomial);
        System.out.println(firstPolynomial.toString());
        System.out.println(secondPolynomial.toString());
        System.out.println(result.toString());
    }

    @Test
    public void addMonomial() {
        Polynomial polynomial = new Polynomial();
        Monomial monomial = new Monomial(3, 4);
        PolynomialOperations.addMonomial(polynomial, monomial);
        Monomial monomial1 = new Monomial(6, 3);
        PolynomialOperations.addMonomial(polynomial, monomial1);
        Monomial monomial2 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(polynomial, monomial2);
        Monomial monomial3 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(polynomial, monomial3);
        Monomial monomial4 = new Monomial(3, 7);
        PolynomialOperations.addMonomial(polynomial, monomial4);
        System.out.println(polynomial.toString());
    }

    @Test
    public void subtract() {
        Polynomial firstPolynomial = new Polynomial();
        Polynomial secondPolynomial = new Polynomial();
        Monomial monomial = new Monomial(3, 4);
        PolynomialOperations.addMonomial(firstPolynomial, monomial);
        Monomial monomial1 = new Monomial(6, 3);
        PolynomialOperations.addMonomial(firstPolynomial, monomial1);
        Monomial monomial2 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(secondPolynomial, monomial2);
        Monomial monomial3 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(secondPolynomial, monomial3);
        Monomial monomial4 = new Monomial(3, 7);
        PolynomialOperations.addMonomial(secondPolynomial, monomial4);
        Polynomial result = PolynomialOperations.subtract(firstPolynomial, secondPolynomial);
        System.out.println(firstPolynomial.toString());
        System.out.println(secondPolynomial.toString());
        System.out.println(result.toString());
    }

    @Test
    public void inverseCoefficients() {
    }
    @Test
    public void getPolynomial() {
        Polynomial polynomial = PolynomialOperations.getPolynomial("4X^5-7X^2+1X^3");
        System.out.println(polynomial.toString());
    }
}