package operations;

import logic.models.Monomial;
import logic.operations.MonomialOperations;
import org.junit.Test;

public class MonomialOperationsTest {

    @Test
    public void add() {
        Monomial monomial1 = new Monomial(4, 5);
        Monomial monomial2 = new Monomial(7, 5);
        MonomialOperations.add(monomial1, monomial2);
        System.out.println(monomial1.toString());
    }
}