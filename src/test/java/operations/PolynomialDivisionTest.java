package operations;

import logic.models.Monomial;
import logic.models.Polynomial;
import logic.operations.PolynomialOperations;
import org.junit.Test;

import java.util.List;

public class PolynomialDivisionTest {

    @Test
    public void divide() {
        Polynomial firstPolynomial = new Polynomial();
        Polynomial secondPolynomial = new Polynomial();
        Monomial monomial = new Monomial(1, 3);
        PolynomialOperations.addMonomial(firstPolynomial, monomial);
        Monomial monomial1 = new Monomial(-12, 2);
        PolynomialOperations.addMonomial(firstPolynomial, monomial1);
        Monomial monomial2 = new Monomial(-42, 0);
        PolynomialOperations.addMonomial(firstPolynomial, monomial2);
        Monomial monomial3 = new Monomial(1, 2);
        PolynomialOperations.addMonomial(secondPolynomial, monomial3);
        Monomial monomial4 = new Monomial(-2, 1);
        PolynomialOperations.addMonomial(secondPolynomial, monomial4);
        Monomial monomial5 = new Monomial(1, 0);
        PolynomialOperations.addMonomial(secondPolynomial, monomial5);
        System.out.println(firstPolynomial.toString());
        System.out.println(secondPolynomial.toString());
        List<Polynomial> polynomials = PolynomialOperations.divide(secondPolynomial, firstPolynomial);
        for (Polynomial polynomial : polynomials) {
            System.out.println(polynomial.toString());
        }
    }
    @Test
    public void divideByConstant() {
        Polynomial firstPolynomial = new Polynomial();
        Polynomial secondPolynomial = new Polynomial();
        Monomial monomial = new Monomial(1, 3);
        PolynomialOperations.addMonomial(firstPolynomial, monomial);
        Monomial monomial1 = new Monomial(-12, 2);
        PolynomialOperations.addMonomial(firstPolynomial, monomial1);
        Monomial monomial2 = new Monomial(-42, 0);
        PolynomialOperations.addMonomial(firstPolynomial, monomial2);
        Monomial monomial3 = new Monomial(3, 0);
        PolynomialOperations.addMonomial(secondPolynomial, monomial3);
        System.out.println(firstPolynomial.toString());
        System.out.println(secondPolynomial.toString());
        List<Polynomial> polynomials = PolynomialOperations.divide(firstPolynomial, secondPolynomial);
        for (Polynomial polynomial : polynomials) {
            System.out.println(polynomial.toString());
        }
    }

}