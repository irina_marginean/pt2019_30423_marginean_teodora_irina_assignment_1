package operations;

import logic.models.Monomial;
import logic.operations.MonomialOperations;
import org.junit.Test;

public class MonomialDivisionTest {

    @Test
    public void divide() {
        Monomial firstMonomial = new Monomial(4, 4);
        Monomial secondMonomial = new Monomial(2, 1);
        Monomial result = MonomialOperations.divide(firstMonomial, secondMonomial);
        System.out.println(result.toString());
    }
    @Test
    public void divideByConstant() {
        Monomial firstMonomial = new Monomial(4, 4);
        Monomial secondMonomial = new Monomial(2, 0);
        Monomial result = MonomialOperations.divide(firstMonomial, secondMonomial);
        System.out.println(result.toString());
    }
}