package operations;

import logic.models.Monomial;
import logic.models.Polynomial;
import logic.operations.PolynomialOperations;
import org.junit.Test;

public class PolynomialMultiplyTest {

    @Test
    public void multiply() {
        Polynomial firstPolynomial = new Polynomial();
        Polynomial secondPolynomial = new Polynomial();
        Monomial monomial = new Monomial(3, 4);
        PolynomialOperations.addMonomial(firstPolynomial, monomial);
        Monomial monomial1 = new Monomial(6, 4);
        PolynomialOperations.addMonomial(firstPolynomial, monomial1);
        Monomial monomial2 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(secondPolynomial, monomial2);
        Monomial monomial3 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(secondPolynomial, monomial3);
        Monomial monomial4 = new Monomial(3, 6);
        PolynomialOperations.addMonomial(secondPolynomial, monomial4);
        System.out.println(firstPolynomial.toString());
        System.out.println(secondPolynomial.toString());
        Polynomial result = PolynomialOperations.multiply(firstPolynomial, secondPolynomial);
        System.out.println(result.toString());
    }
}