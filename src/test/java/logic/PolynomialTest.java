package logic;

import logic.models.Monomial;
import logic.models.Polynomial;
import logic.operations.PolynomialOperations;
import org.junit.Test;

public class PolynomialTest {

    @Test
    public void toStringTest() {
        Polynomial firstPolynomial = new Polynomial();
        Monomial monomial = new Monomial(3, 4);
        PolynomialOperations.addMonomial(firstPolynomial, monomial);
        Monomial monomial1 = new Monomial(6, 4);
        PolynomialOperations.addMonomial(firstPolynomial, monomial1);
        Monomial monomial2 = new Monomial(3, 3);
        PolynomialOperations.addMonomial(firstPolynomial, monomial2);
        Monomial monomial3 = new Monomial(-7, 3);
        PolynomialOperations.addMonomial(firstPolynomial, monomial3);
        Monomial monomial5 = new Monomial(-9, 1);
        PolynomialOperations.addMonomial(firstPolynomial, monomial5);
        Monomial monomial4 = new Monomial(-5, 0);
        PolynomialOperations.addMonomial(firstPolynomial, monomial4);
        System.out.println(firstPolynomial.toString());
    }

    @Test
    public void deepCopy() {
        Polynomial polynomial = new Polynomial();
        Polynomial polynomialCopy = polynomial.deepCopy();
        polynomial.getMonomials().add(new Monomial(6, 7));
        polynomialCopy.getMonomials().add(new Monomial(3, 4));
        polynomial.sort();
        polynomialCopy.sort();
        System.out.println(polynomial.toString());
        System.out.println(polynomialCopy.toString());
    }

}