package logic.operations;

import logic.models.Polynomial;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class OperationsTest {

    @Test
    public void add() {
        Polynomial firstPolynomial = PolynomialOperations.getPolynomial("3X^5+7X^7");
        Polynomial secondPolynomial = PolynomialOperations.getPolynomial("9X^5+9X^0+0X^8");
        Polynomial result = PolynomialOperations.add(firstPolynomial, secondPolynomial);
        assertEquals("7.0X^7+12.0X^5+9.0X^0", result.toString());
    }

    @Test
    public void subtract() {
        Polynomial firstPolynomial = PolynomialOperations.getPolynomial("3X^5+7X^7");
        Polynomial secondPolynomial = PolynomialOperations.getPolynomial("9X^5+9X^0+0X^8");
        Polynomial result = PolynomialOperations.subtract(firstPolynomial, secondPolynomial);
        assertEquals("7.0X^7-6.0X^5-9.0X^0", result.toString());
    }

    @Test
    public void integrate() {
        Polynomial firstPolynomial = PolynomialOperations.getPolynomial("6X^5+16X^7");
        Polynomial result = PolynomialOperations.integrate(firstPolynomial);
        assertEquals("2.0X^8+1.0X^6", result.toString());
    }

    @Test
    public void differentiate() {
        Polynomial firstPolynomial = PolynomialOperations.getPolynomial("3X^5+7X^7");
        Polynomial result = PolynomialOperations.differentiate(firstPolynomial);
        assertEquals("49.0X^6+15.0X^4", result.toString());
    }

    @Test
    public void multiply() {
        Polynomial firstPolynomial = PolynomialOperations.getPolynomial("3X^5+7X^7");
        Polynomial secondPolynomial = PolynomialOperations.getPolynomial("9X^5+9X^0");
        Polynomial result = PolynomialOperations.multiply(firstPolynomial, secondPolynomial);
        assertEquals("63.0X^12+27.0X^10+63.0X^7+27.0X^5", result.toString());
    }

    @Test
    public void divide() {
        Polynomial firstPolynomial = PolynomialOperations.getPolynomial("3X^5+7X^7");
        Polynomial secondPolynomial = PolynomialOperations.getPolynomial("9X^5+6X^3");
        List<Polynomial> result = PolynomialOperations.divide(firstPolynomial, secondPolynomial);
        assertEquals("0.8X^2-0.2X^0", result.get(0).toString());
        assertEquals("1.1X^3", (result.get(1)).toString());
    }
}