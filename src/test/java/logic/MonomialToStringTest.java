package logic;

import logic.models.Monomial;
import org.junit.Test;

public class MonomialToStringTest {

    @Test
    public void toStringTest() {
        Monomial monomial = new Monomial();
        monomial.setCoefficient(1);
        monomial.setDegree(1);
        System.out.println(monomial.toString());
    }
    @Test
    public void monomial() {
        String text = "-3X^5";
        Monomial monomial = new Monomial(text);
        System.out.println(monomial.toString());
    }
}