package views;

import javax.swing.*;
import java.awt.event.ActionListener;

public class OperationsView extends JFrame {
    private JTextField firstPolynomialTextField = new JTextField();
    private JTextField secondPolynomialTextField = new JTextField();
    private JTextField resultTextField = new JTextField();
    private JTextField restTextField = new JTextField();
    private JLabel firstPolynomialLabel = new JLabel();
    private JLabel secondPolynomialLabel = new JLabel();
    private JLabel resultLabel = new JLabel();
    private JLabel restLabel = new JLabel();
    private JButton addButton = new JButton();
    private JButton subtractButton = new JButton();
    private JButton multiplyButton = new JButton();
    private JButton divideButton = new JButton();
    private JButton differentiateButton = new JButton();
    private JButton integrateButton = new JButton();

    public OperationsView() {
        int x = 50;
        int y = 30;
        int width = 400;
        int height = 33;
        this.setTitle("Polynomial operations");
        this.setBounds(1200, 800, 800, 480);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        firstPolynomialLabel.setBounds(x, y, width, height);
        firstPolynomialLabel.setText("The first polynomial is: ");
        getContentPane().add(firstPolynomialLabel);

        firstPolynomialTextField.setBounds(x, y + 40, width, height);
        getContentPane().add(firstPolynomialTextField);

        secondPolynomialLabel.setBounds(x, y + 90, width, height);
        secondPolynomialLabel.setText("The second polynomial is: ");
        getContentPane().add(secondPolynomialLabel);

        secondPolynomialTextField.setBounds(x, y + 130, width, height);
        getContentPane().add(secondPolynomialTextField);

        resultLabel.setBounds(x, y + 180, width, height);
        resultLabel.setText("The result is: ");
        getContentPane().add(resultLabel);

        resultTextField.setBounds(x, y + 220, width, height);
        getContentPane().add(resultTextField);

        restLabel.setBounds(x, y + 270, width, height);
        restLabel.setText("The remainder is: ");
        getContentPane().add(restLabel);

        restTextField.setBounds(x, y + 310, width, height);
        getContentPane().add(restTextField);

        int buttonX = 550;
        int buttonY = 50;
        int buttonWidth = 180;
        int buttonHeight = 50;

        addButton.setBounds(buttonX, buttonY, buttonWidth, buttonHeight);
        addButton.setText("Add");
        getContentPane().add(addButton);

        subtractButton.setBounds(buttonX, buttonY + buttonHeight + 5, buttonWidth, buttonHeight);
        subtractButton.setText("Subtract");
        getContentPane().add(subtractButton);

        multiplyButton.setBounds(buttonX, buttonY + 2 * (buttonHeight + 5), buttonWidth, buttonHeight);
        multiplyButton.setText("Multiply");
        getContentPane().add(multiplyButton);

        divideButton.setBounds(buttonX, buttonY + 3 * (buttonHeight + 5), buttonWidth, buttonHeight);
        divideButton.setText("Divide");
        getContentPane().add(divideButton);

        differentiateButton.setBounds(buttonX, buttonY + 4 * (buttonHeight + 5), buttonWidth, buttonHeight);
        differentiateButton.setText("Differentiate");
        getContentPane().add(differentiateButton);

        integrateButton.setBounds(buttonX, buttonY + 5 * (buttonHeight + 5), buttonWidth, buttonHeight);
        integrateButton.setText("Integrate");
        getContentPane().add(integrateButton);
    }
    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }
    public String getFirstPolynomialText() {
         if (firstPolynomialTextField.getText().equals("")) {
             showWarningMessage("You did not input the first polynomial");
             return "";
         }
         return firstPolynomialTextField.getText();
    }
    public String getSecondPolynomialText() {
        if (secondPolynomialTextField.getText().equals("")) {
            showWarningMessage("You did not input the second polynomial");
        }
        return secondPolynomialTextField.getText();
    }
    public void setResultTextField(String text) {
        resultTextField.setText(text);
    }
    public void setRestTextField(String text) {
        restTextField.setText(text);
    }
    public void addAddButtonListener(final ActionListener actionListener) {
            addButton.addActionListener(actionListener);
    }
    public void addSubtractButtonListener(final ActionListener actionListener) {
        subtractButton.addActionListener(actionListener);
    }
    public void addMultiplyButtonListener(final ActionListener actionListener) {
        multiplyButton.addActionListener(actionListener);
    }
    public void addDivideButtonListener(final ActionListener actionListener) {
        divideButton.addActionListener(actionListener);
    }
    public void addDifferentiateButtonListener(final ActionListener actionListener) {
        differentiateButton.addActionListener(actionListener);
    }
    public void addIntegrateButtonListener(final ActionListener actionListener) {
        integrateButton.addActionListener(actionListener);
    }
}
