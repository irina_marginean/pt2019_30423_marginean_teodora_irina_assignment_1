package logic.operations;

import logic.models.Monomial;

public class MonomialOperations {
    public static Monomial add(Monomial firstMonomial, Monomial secondMonomial) {
        Monomial result = new Monomial(firstMonomial);
        result.setCoefficient(firstMonomial.getCoefficient() + secondMonomial.getCoefficient());
        return result;
    }
    public static Monomial integrate(Monomial monomial) {
        Monomial result = new Monomial();
        result.setDegree(monomial.getDegree() + 1);
        result.setCoefficient(monomial.getCoefficient() * (1.0 / result.getDegree()));
        return result;
    }
    public static Monomial differentiate(Monomial monomial) {
        Monomial result = new Monomial();
        result.setDegree(monomial.getDegree() - 1);
        result.setCoefficient(monomial.getCoefficient() * monomial.getDegree());
        return result;
    }
    public static Monomial multiply(Monomial firstMonomial, Monomial secondMonomial) {
        Monomial result = new Monomial(firstMonomial);
        result.setDegree(firstMonomial.getDegree() + secondMonomial.getDegree());
        result.setCoefficient(firstMonomial.getCoefficient() * secondMonomial.getCoefficient());
        return result;
    }
    public static Monomial divide(Monomial firstMonomial, Monomial secondMonomial) {
        Monomial result = new Monomial(firstMonomial);
        result.setDegree(firstMonomial.getDegree() - secondMonomial.getDegree());
        result.setCoefficient(firstMonomial.getCoefficient() / secondMonomial.getCoefficient());
        return  result;
    }
}
