package logic.operations;

import exceptions.NullDivisionException;
import logic.models.Monomial;
import logic.models.Polynomial;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialOperations {
    public static Polynomial add(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        Polynomial result = firstPolynomial.deepCopy();
        for (Monomial monomial : secondPolynomial.getMonomials()) {
            addMonomial(result, monomial);
            result.sort();
        }
        removeZeroes(result);
        return result;
    }

    public static Polynomial subtract(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        inverseCoefficients(secondPolynomial);
        Polynomial result = add(firstPolynomial, secondPolynomial);
        return result;
    }
    public static Polynomial integrate(Polynomial polynomial) {
        Polynomial copyPolynomial = polynomial.deepCopy();
        for (Monomial monomial : copyPolynomial.getMonomials()) {
            monomial.deepCopy(MonomialOperations.integrate(monomial));
        }
        removeZeroes(copyPolynomial);
        return  copyPolynomial;
    }

    public static Polynomial differentiate(Polynomial polynomial) {
        Polynomial copyPolynomial = polynomial.deepCopy();
        for (Monomial monomial : copyPolynomial.getMonomials()) {
            monomial.deepCopy(MonomialOperations.differentiate(monomial));
        }
        removeZeroes(copyPolynomial);
        return copyPolynomial;
    }

    public static Polynomial multiply(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        Polynomial result = new Polynomial();
        for (Monomial firstMonomial : firstPolynomial.getMonomials()) {
            for (Monomial secondMonomial : secondPolynomial.getMonomials()) {
                Monomial multipliedMonomial = MonomialOperations.multiply(firstMonomial, secondMonomial);
                addMonomial(result, multipliedMonomial);
            }
        }
        result.sort();
        return result;
    }

    // helper and helperPolynomial are used for intermediary computations
    // based on Horner's method
    // it treats the case when the second polynomial is a constant separately
    public static List<Polynomial> divide(Polynomial firstPolynomial, Polynomial secondPolynomial) throws NullDivisionException {
        if (secondPolynomial.getMonomials().size() == 1 && getHighestDegreeMonomial(secondPolynomial).getCoefficient() == 0) {
            throw new NullDivisionException("Cannot divide by 0!");
        } else {
            List<Polynomial> result = new ArrayList<>();
            Polynomial quotient = new Polynomial();
            Polynomial rest = new Polynomial();
            Polynomial dividend = firstPolynomial.deepCopy();
            Polynomial divisor = secondPolynomial.deepCopy();
            if (getHighestDegreeMonomial(divisor).getDegree() != 0) {
                while (getHighestDegreeMonomial(dividend).getDegree() >= getHighestDegreeMonomial(divisor).getDegree()) {
                    Monomial helper = MonomialOperations.divide(getHighestDegreeMonomial(dividend),
                            getHighestDegreeMonomial(divisor));
                    addMonomial(quotient, helper);
                    Polynomial polynomialHelper = multiply(divisor, helper);
                    dividend = subtract(dividend, polynomialHelper);
                }
                rest = dividend;
            } else {
                for (Monomial monomial : dividend.getMonomials()) {
                    Monomial helper = MonomialOperations.divide(monomial, getHighestDegreeMonomial(divisor));
                    addMonomial(quotient, helper);
                }
            }
            result.add(quotient);
            result.add(rest);
            return result;
        }
    }

    // adds a monomial to the polynomial
    public static void addMonomial(Polynomial polynomial, Monomial monomial) {
        if (!polynomial.getMonomials().isEmpty()) {
            List<Monomial> monomialList = new ArrayList<>(polynomial.getMonomials());
            for (Monomial currentMonomial : monomialList) {
                if (currentMonomial.getDegree() == monomial.getDegree()) {
                    currentMonomial.deepCopy(MonomialOperations.add(currentMonomial, monomial));
                }
                else {
                    if (!existsSameDegree(polynomial, monomial)) {
                        polynomial.getMonomials().add(monomial);
                        polynomial.sort();
                    }
                }
            }
        }
        else {
            polynomial.getMonomials().add(monomial);
        }
    }

    // checks if the polynomial contains a term having the same degree as the monomial
    public static boolean existsSameDegree(Polynomial polynomial, Monomial monomial) {
        for (Monomial auxMonomial : polynomial.getMonomials()) {
            if (auxMonomial.getDegree() == monomial.getDegree()) {
                return true;
            }
        }
        return false;
    }

    protected static void inverseCoefficients(Polynomial polynomial) {
        for (Monomial monomial : polynomial.getMonomials()) {
            double currentCoefficient = monomial.getCoefficient();
            monomial.setCoefficient((-1) * currentCoefficient);
        }
    }

    // removes all null elements from a polynomial
    protected  static  void removeZeroes(Polynomial polynomial) {
        List<Monomial> monomialList = new ArrayList<>(polynomial.getMonomials());
        for (Monomial monomial : monomialList) {
            if (monomial.getCoefficient() == 0) {
                polynomial.getMonomials().remove(monomial);
            }
        }
    }
    // multiplies a polynomial by a monomial
    protected static Polynomial multiply(Polynomial polynomial, Monomial monomial) {
        Polynomial result = new Polynomial();
        for (Monomial currentMonomial : polynomial.getMonomials()) {
            Monomial multipliedMonomial = MonomialOperations.multiply(currentMonomial, monomial);
            addMonomial(result, multipliedMonomial);
        }
        removeZeroes(result);
        return result;
    }
    // gets the lead term of a polynomial
    public static Monomial getHighestDegreeMonomial(Polynomial polynomial) {
        // gets the first element in the monomial list, because it is always sorted
        if (polynomial.getMonomials().isEmpty()) {
            return new Monomial(0, 0);
        }
        else {
            return polynomial.getMonomials().get(0);
        }
    }

    public static Polynomial getPolynomial(String text) throws NumberFormatException {
        // constructs the polynomial based on the regex
        // throws a NumberFormatException further to be handled by the controller and the UI
        Polynomial polynomial = new Polynomial();
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
        Matcher matcher = pattern.matcher(text);
        int x = 0;
        while (matcher.find()) {
            Monomial monomial = new Monomial(matcher.group(1));
            PolynomialOperations.addMonomial(polynomial, monomial);
        }
        return polynomial;
    }
}
