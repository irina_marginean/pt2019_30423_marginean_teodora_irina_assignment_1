package logic.models;

import java.util.*;

public class Polynomial {
    // in my design, polynomials are implemented by using lists of monomials
    private List<Monomial> monomials;
    public List<Monomial> getMonomials() {
        return monomials;
    }
    public void setMonomials(List<Monomial> monomials) {
        this.monomials = monomials;
    }
    public Polynomial()
    {
        monomials = new ArrayList<>();
    }
    public Polynomial(List<Monomial> monomials) {
        this.monomials = monomials;
    }
    public Polynomial deepCopy()
    {
        // method necessary to correctly copy a polynomial
        List<Monomial> monomialsClone = new ArrayList<>();
        for (Monomial monomial : monomials) {
                monomialsClone.add(new Monomial(monomial));
        }
        Polynomial polynomial = new Polynomial(monomialsClone);
        return polynomial;
    }
    // the sort method sorts the list of monomials by their degree
    public void sort()
    {
        Collections.sort(monomials);
    }
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        // if the polynomial is empty, it returns "0X^0"
        //else, it constructs the string by checking if the coefficient is positive or negative
        if (!monomials.isEmpty()) {
            for (Monomial monomial : monomials)
            {
                if (monomial.getCoefficient() >= 0) {
                    stringBuilder.append("+");
                    stringBuilder.append(monomial.toString());
                }
                else {
                    stringBuilder.append("-");
                    Monomial absoluteValueMonomial = new Monomial(monomial);
                    absoluteValueMonomial.setCoefficient( (-1) * absoluteValueMonomial.getCoefficient());
                    stringBuilder.append(absoluteValueMonomial.toString());
                }
            }
        }
        else {
            return "0X^0";
        }
        // the previous for appends + or - at the start of the string
        // this instruction deletes the + or -
        if (monomials.get(0).getCoefficient() > 0) {
            stringBuilder.delete(0, 1);
        }
        return  stringBuilder.toString();
    }
}
