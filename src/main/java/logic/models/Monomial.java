package logic.models;

import org.jetbrains.annotations.NotNull;

public class Monomial implements Comparable<Monomial>{
    private double coefficient;
    private int degree;
    public double getCoefficient() { return coefficient; }
    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }
    public int getDegree() {
        return degree;
    }
    public void setDegree(int degree) {
        this.degree = degree;
    }
    public Monomial() { }
    public  Monomial(Monomial monomial) {
        // constructs a copy of the argument
        coefficient = monomial.coefficient;
        degree = monomial.degree;
    }
    public Monomial(double coefficient, int degree) {
        this.coefficient = coefficient;
        this.degree = degree;
    }
    public Monomial(String text) throws NumberFormatException {
        // it is used to read monomials from the input
        // it throws the NumberFormatException if the input does not correspond to a monomial
        String textCopy = new String(text);
        // the index of the ^ is used as a reference point
        int indexOfCaret = textCopy.indexOf('^');
        this.degree = Integer.parseInt(text.substring(indexOfCaret + 1, textCopy.length()));
        this.coefficient = Double.parseDouble(text.substring(0, indexOfCaret - 1));
    }

    // modifies the fields of the current object according to Monomial given as parameter
    public void deepCopy(Monomial monomial) {
        coefficient = monomial.coefficient;
        degree = monomial.degree;
    }
    @Override
    public String toString() {
        return String.format("%.1f", coefficient) + "X^" + degree;
    }
    // implements the compareTo method, as the class itself implements the Comparable interface
    @Override
    public int compareTo(@NotNull Monomial o) {
        return o.getDegree() - this.getDegree();
    }
}
