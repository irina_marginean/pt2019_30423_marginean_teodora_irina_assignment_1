package controllers;

import exceptions.NullDivisionException;
import logic.models.Polynomial;
import logic.operations.PolynomialOperations;
import views.OperationsView;

import java.util.List;

import javax.swing.*;

// binds the view to the logic
public class MainController {
    private OperationsView operationsView;
    public void start() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e)
        {
            System.out.println("Error");
        }
        operationsView = new OperationsView();
        operationsView.setLocationRelativeTo(null);
        operationsView.setVisible(true);
        initializeButtonListeners();
    }
    private void initializeButtonListeners() {
        //reads the first and the second polynomial and adds them in the result field
        operationsView.addAddButtonListener(e -> {
            //the try-catch block is used to make sure the input is correct
            try {
                String firstPolynomialText = operationsView.getFirstPolynomialText();
                //reads polynomials by using a regex in the getPolynomial method
                Polynomial firstPolynomial = PolynomialOperations.getPolynomial(firstPolynomialText);
                String secondPolynomialText = operationsView.getSecondPolynomialText();
                Polynomial secondPolynomial = PolynomialOperations.getPolynomial(secondPolynomialText);
                Polynomial result = PolynomialOperations.add(firstPolynomial, secondPolynomial);
                operationsView.setResultTextField(result.toString());
                //makes sure the rest field is not displayed (because this operation does not use it)
                operationsView.setRestTextField("");
            }
            catch (NumberFormatException w) {
                operationsView.showWarningMessage("You did not input a valid polynomial!");
            }
        });
        //similar to the addition procedure
        operationsView.addSubtractButtonListener(e -> {
            try {
                String firstPolynomialText = operationsView.getFirstPolynomialText();
                Polynomial firstPolynomial = PolynomialOperations.getPolynomial(firstPolynomialText);
                String secondPolynomialText = operationsView.getSecondPolynomialText();
                Polynomial secondPolynomial = PolynomialOperations.getPolynomial(secondPolynomialText);
                Polynomial result = PolynomialOperations.subtract(firstPolynomial, secondPolynomial);
                operationsView.setResultTextField(result.toString());
                operationsView.setRestTextField("");
            }
            catch (NumberFormatException w) {
                operationsView.showWarningMessage("You did not input a valid polynomial!");
            }
        });
        //similar to the addition procedure
        operationsView.addMultiplyButtonListener(e -> {
            try {
                String firstPolynomialText = operationsView.getFirstPolynomialText();
                Polynomial firstPolynomial = PolynomialOperations.getPolynomial(firstPolynomialText);
                String secondPolynomialText = operationsView.getSecondPolynomialText();
                Polynomial secondPolynomial = PolynomialOperations.getPolynomial(secondPolynomialText);
                Polynomial result = PolynomialOperations.multiply(firstPolynomial, secondPolynomial);
                operationsView.setResultTextField(result.toString());
                operationsView.setRestTextField("");
            }
            catch (NumberFormatException w) {
                operationsView.showWarningMessage("You did not input a valid polynomial!");
            }

        });
        //similar to the addition procedure
        operationsView.addDivideButtonListener(e -> {
            try
            {
                String firstPolynomialText = operationsView.getFirstPolynomialText();
                Polynomial firstPolynomial = PolynomialOperations.getPolynomial(firstPolynomialText);
                String secondPolynomialText = operationsView.getSecondPolynomialText();
                Polynomial secondPolynomial = PolynomialOperations.getPolynomial(secondPolynomialText);
                // the try-catch block makes sure that no division by 0 happens
                try {
                    List<Polynomial> results = PolynomialOperations.divide(firstPolynomial, secondPolynomial);
                    operationsView.setResultTextField(results.get(0).toString());
                    // here the remainder field is used as well
                    operationsView.setRestTextField(results.get(1).toString());
                }
                catch (NullDivisionException t) {
                    operationsView.showWarningMessage("Cannot divide by 0!");
                }
            }
            catch (NumberFormatException w) {
                operationsView.showWarningMessage("You did not input a valid polynomial!");
            }
        });
        //similar to the addition procedure
        operationsView.addDifferentiateButtonListener(e -> {
            try {
                String firstPolynomialText = operationsView.getFirstPolynomialText();
                Polynomial firstPolynomial = PolynomialOperations.getPolynomial(firstPolynomialText);
                Polynomial result = PolynomialOperations.differentiate(firstPolynomial);
                // it only uses the first polynomial as input
                operationsView.setResultTextField(result.toString());
                operationsView.setRestTextField("");
            }
            catch (NumberFormatException w) {
                operationsView.showWarningMessage("You did not input a valid polynomial!");
            }
        });
        // similar to the differentiation procedure
        operationsView.addIntegrateButtonListener(e -> {
            try {
                String firstPolynomialText = operationsView.getFirstPolynomialText();
                Polynomial firstPolynomial = PolynomialOperations.getPolynomial(firstPolynomialText);
                Polynomial result = PolynomialOperations.integrate(firstPolynomial);
                operationsView.setResultTextField(result.toString());
                operationsView.setRestTextField("");
            }
            catch (NumberFormatException w) {
                operationsView.showWarningMessage("You did not input a valid polynomial!");
            }
        });
    }
}
