import controllers.MainController;

public class Application {
    public static void main(String args[]) {
        MainController controller = new MainController();
        controller.start();
    }
}
